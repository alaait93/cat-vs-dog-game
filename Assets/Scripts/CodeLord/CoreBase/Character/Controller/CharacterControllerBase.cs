﻿using System.Collections;
using UnityEngine;
using CodeLord.Utils;

namespace CodeLord.CatVsDog
{
    public abstract class CharacterControllerBase : MonoBehaviour
    {
        public enum CharacterType {Player, AI};
        public CharacterType characterType;
        protected bool isMyTurn = false;
        protected SlingshotWeapon weaponController;
        private bool canDrag = true;
        protected Collider2D characterCollider;

        protected virtual void Awake()
        {
            SetColliderSettings();
        }

        protected virtual void Start()
        {
            DefinePlayerBehaviour();
            FindWeaponAbility();
        }

        /// <summary>
        /// Define how player is going to act based on its type (AI or real player)
        /// </summary>
        protected virtual void DefinePlayerBehaviour()
        {
            if (characterType == CharacterType.Player)
            {
                RegisterToInputEvents();
            }
        }

        protected virtual void OnDisable()
        {
            if (characterType == CharacterType.Player)
                UnregisterToInputEvents();
        }

        protected virtual void RegisterToInputEvents()
        {
            KeyboardInput.OnMouseStartTriggered += KeyboardInput_OnMouseStart;
            KeyboardInput.OnMouseDownTriggered += KeyboardInput_OnMouseDown;
            KeyboardInput.OnMouseEndTriggered += KeyboardInput_OnMouseEnd;
        }

        protected virtual void UnregisterToInputEvents()
        {
            KeyboardInput.OnMouseStartTriggered -= KeyboardInput_OnMouseStart;
            KeyboardInput.OnMouseDownTriggered -= KeyboardInput_OnMouseDown;
            KeyboardInput.OnMouseEndTriggered -= KeyboardInput_OnMouseEnd;
        }

        protected virtual void SetColliderSettings()
        {
            characterCollider = this.GetComponent<Collider2D>();
            
            if (!characterCollider.isTrigger)
            {
                characterCollider.isTrigger = true;
            }
        }

        protected virtual void FindWeaponAbility()
        {
            weaponController = this.GetComponentInChildren<SlingshotWeapon>();
        }

        /// <summary>
        /// Once mouse clicked we detect the click start event
        /// </summary>
        protected virtual void KeyboardInput_OnMouseStart(Vector3 mousePosition)
        {
            if (canDrag && isMyTurn)
                weaponController.SetSlingshotColor(Color.white);
        } 

        /// <summary>
        /// While mouse button is down we keep track of mouseDown event until its released
        /// </summary>
        protected virtual void KeyboardInput_OnMouseDown(Vector3 mousePosition)
        {
            mousePosition = new Vector3(mousePosition.x, mousePosition.y, mousePosition.z);

            if(canDrag && isMyTurn)
                OnDrag(mousePosition);
        } 

        /// <summary>
        /// Once mouse released we detect the click end event
        /// </summary>
        protected virtual void KeyboardInput_OnMouseEnd(Vector3 mousePosition)
        {
            mousePosition = new Vector3(mousePosition.x, mousePosition.y, mousePosition.z);

            if(canDrag && isMyTurn)
                OnRelease(mousePosition);
        }

        protected IEnumerator Fire(Vector3 target)
        {
            weaponController.CheckThrowAvailability(target, OnFireTriggered);
            yield return new WaitForSeconds(2f);
        }

        public void OnDrag(Vector3 mousePosition, bool isAI = false)
        { 
            Vector3 worldPosition = Vector3.zero;

            if (!isAI)
                worldPosition = CLUtils.ConvertScreenToWorldPoint(mousePosition, GamePlayManagerBase.Instance.cameraController.GetCamera());
            else
                worldPosition = mousePosition;

            if (isMyTurn)
            {
                weaponController.RotateToTarget(worldPosition, isAI);
            }
        }

        public void OnRelease(Vector3 mousePosition)
        {
            if (isMyTurn)
            {
                Vector3 worldPosition = CLUtils.ConvertScreenToWorldPoint(mousePosition, GamePlayManagerBase.Instance.cameraController.GetCamera());
                canDrag = false;
                StartCoroutine(Fire(worldPosition));
            }
        }

        /// <summary>
        /// A delegate finction which will be called after done with slingshot to notify CharacterControllerBase if throwing weapon success to switch turns or not to keep waiting
        /// </summary>
        /// <param name="isFireSuccess"></param>
        private void OnFireTriggered(bool isFireSuccess)
        {
            canDrag = !isFireSuccess;
            characterCollider.enabled = canDrag;

            if (isFireSuccess)
            {
                StartCoroutine(OnPostFire(2f));
            }
        }

        /// <summary>
        /// After successfully throwing weapon hide the slingshot trajectory line and notify GameManager to switch turns
        /// </summary>
        /// <param name="delay"></param>
        /// <returns></returns>
        private IEnumerator OnPostFire(float delay)
        {
            yield return new WaitForSeconds(delay);
            isMyTurn = false;
            weaponController.SetSlingshotVisibilityState(false);
            GamePlayManagerBase.Instance.SwitchTurn();
        }
         
        /// <summary>
        /// Act if current turn is my turn
        /// </summary>
        public virtual void MyTurn()
        {
            isMyTurn = true;
            canDrag = true;// so we can listen to mouse events again in the next turn
            characterCollider.enabled = canDrag;
            OnTurnChanged?.Invoke(isMyTurn);
        }
        public void InitCharacterView(bool isLeft)
        {
            this.GetComponentInChildren<CharacterViewBase>().FlipSpriteX(isLeft);

            if (isLeft)
            {
                this.GetComponentInChildren<CharacterViewBase>().SetTint(Color.red);
                this.GetComponentInChildren<SlingshotView>().SetTint(Color.red);
            }
        }

        #region Character Events
        public delegate void TurnChanged(bool myTurn);
        public delegate void FireTriggered(bool isFireSuccess);
        public event TurnChanged OnTurnChanged;
        #endregion
    }
}
