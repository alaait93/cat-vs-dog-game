﻿using UnityEngine;

namespace CodeLord.CatVsDog
{
    [RequireComponent (typeof(SpriteRenderer))]
    [RequireComponent(typeof(Animator))]
    public abstract class CharacterViewBase : MonoBehaviour
    {
        protected SpriteRenderer spriteRenderer;
        protected Animator animator;

        protected void Awake()
        {
            spriteRenderer = this.GetComponent<SpriteRenderer>();
            animator = this.GetComponent<Animator>();
        }

        public void FlipSpriteX(bool state)
        {
            spriteRenderer.flipX = state;
        }

        public void SetTint(Color color)
        {
            spriteRenderer.color = color;
        }
    }
}
