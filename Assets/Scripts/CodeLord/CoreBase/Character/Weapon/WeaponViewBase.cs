﻿using UnityEngine;

namespace CodeLord.CatVsDog
{
    [RequireComponent (typeof(SpriteRenderer))]
    [RequireComponent(typeof(Animator))]
    public abstract class WeaponViewBase : MonoBehaviour
    {
        protected SpriteRenderer spriteRenderer;
        protected Animator animator;

        protected virtual void Awake()
        {
            spriteRenderer = this.GetComponent<SpriteRenderer>();
            animator = this.GetComponent<Animator>();
        }
    }
}
