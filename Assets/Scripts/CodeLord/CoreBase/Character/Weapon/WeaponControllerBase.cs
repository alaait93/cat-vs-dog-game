﻿using UnityEngine;

namespace CodeLord.CatVsDog
{
    public abstract class WeaponControllerBase : MonoBehaviour
    {
        public abstract void RotateToTarget(Vector3 targetDirection, bool reveredShot = false);

        public abstract void FireWeapon(Vector3 throwDirection, bool reveredShot = false);

        public abstract void CheckThrowAvailability(Vector3 dragDirection, CharacterControllerBase.FireTriggered fireTriggered, bool reversedShot = false);
    }
}
