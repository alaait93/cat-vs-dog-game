﻿using UnityEngine;

namespace CodeLord.CatVsDog
{
    public abstract class InputBase : MonoBehaviour
    {
        protected bool inputDetected = false;

        protected virtual void OnMouseDown()
        {
            inputDetected = true;
            OnInputStarted();
        }
        protected virtual void OnMouseUp()
        {
            inputDetected = false;
            OnInputEnded();
        }
        protected virtual void Update()
        {
            if (inputDetected)
                OnInput();
        }

        protected abstract void OnInputStarted();
        protected abstract void OnInput();
        protected abstract void OnInputEnded();
        
    }
}
