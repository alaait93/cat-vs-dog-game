﻿using UnityEngine;
using CodeLord.Utils;

namespace CodeLord.CatVsDog
{
    public abstract class GamePlayManagerBase : MonoBehaviour
    {
        private static GamePlayManagerBase _instance;
        public static GamePlayManagerBase Instance
        {
            get
            {
                if (_instance == null)
                    _instance = (GamePlayManagerBase) GameObject.FindObjectOfType<GamePlayManagerBase>();

                return _instance;
            }
        }

        public Transform[] playerInitPosition;

        public CharacterControllerBase player1;
        public CharacterControllerBase player2;
        public GameObject roleArrow;

        [HideInInspector]
        public ScoreControllerBase scoreController;
        [HideInInspector]
        public SoundControllerBase soundController;
        [HideInInspector]
        public UIControllerBase uIController;
        [HideInInspector]
        public DebugController debugController;        
        [HideInInspector]
        public CameraController cameraController;
        
        protected int roleId = 0;

        
        protected virtual void Awake()
        {
            Application.targetFrameRate = 300;
            InitReferences();
            DefinePlayers();
        }

        protected void Start()
        {
            StartGame();
        }

        protected virtual void InitReferences()
        {
            scoreController = GameObject.FindObjectOfType<ScoreControllerBase>();
            soundController = GameObject.FindObjectOfType<SoundControllerBase>();

            uIController = GameObject.FindObjectOfType<UIControllerBase>();
            debugController = GameObject.FindObjectOfType<DebugController>();
            cameraController = GameObject.FindObjectOfType<CameraController>();
        }
        
        protected void DefinePlayers()
        {
            GameObject player = Resources.Load<GameObject>("Characters/cat/catCharacter");

            player1 = Instantiate(player).GetComponent<CharacterControllerBase>();

            player1.transform.position = playerInitPosition[0].position;

            player1.name = "Player Character";

            player2 = Instantiate(player).GetComponent<CharacterControllerBase>();

            player2.name = "AI Character";
            player2.characterType = CharacterControllerBase.CharacterType.AI;
            player2.InitCharacterView(true);
            player2.gameObject.AddComponent<AICharacterController>();

            player2.transform.position = playerInitPosition[1].position;
        }

        public void SwitchTurn()
        {
            if (roleId != 0) // change to player 1
            {
                roleId = 0;
                player1.MyTurn();
                roleArrow.transform.parent = player1.transform;
            }

            else // change to player 2
            {
                roleId = 1;
                player2.MyTurn();
                roleArrow.transform.parent = player2.transform;
            }

        }

        public void StartGame()
        {
            roleId = -1;
            SwitchTurn();
        }

    }
}
