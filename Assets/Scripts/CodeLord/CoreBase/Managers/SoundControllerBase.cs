﻿using UnityEngine;

namespace CodeLord.CatVsDog
{
    public abstract class SoundControllerBase : MonoBehaviour
    {
        protected AudioSource audioLoop;
        protected AudioSource ambienceLoop;

        protected virtual void Awake()
        {
            audioLoop = this.GetComponent<AudioSource>();
        }

        protected virtual void Start()
        {
            PlayAudioLoop();
        }
        protected void PlayAudioLoop()
        {
            audioLoop.Play();
        }

        protected void StopAudioLoop()
        {
            audioLoop.Stop();
        }
    }
}
