﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CodeLord.CatVsDog
{
    public class KeyboardInput : InputBase
    {
        public bool mouseStart = false;
        public bool mouseDown = false;

        protected override void OnInputStarted()
        {
            mouseStart = true;

            if (OnMouseStartTriggered != null)
                OnMouseStartTriggered(GetRawMousePosition());
        }

        protected override void OnInput()
        {
            mouseDown = true;

            if (OnMouseDownTriggered != null)
            {
                //Debug.Log("On Mouse Down");
                OnMouseDownTriggered(GetRawMousePosition());
            }
        }

        protected override void OnInputEnded()
        {
            mouseStart = false;

            if (OnMouseEndTriggered != null)
                OnMouseEndTriggered(GetRawMousePosition());
        }

        /// <summary>
        /// Get Mouse Position in world space
        /// </summary>
        /// <returns></returns>
        private Vector3 GetCurrentMousePosition()
        {
            Vector3 currentMousePosition = Input.mousePosition;
            currentMousePosition = Camera.main.ScreenToWorldPoint(currentMousePosition);

            return currentMousePosition;
        }

        /// <summary>
        /// Get the row mouse position in screen space
        /// </summary>
        /// <returns></returns>
        private Vector3 GetRawMousePosition()
        {
            Vector3 currentMousePosition = Input.mousePosition;

            return currentMousePosition;
        }

        #region Keyboard Events
        /// <summary>
        /// Once mouse clicked we detect the click start event
        /// </summary>
        public delegate void MouseStart(Vector3 mousePosition);
        public static event MouseStart OnMouseStartTriggered;

        /// <summary>
        /// Once mouse released we detect the click end event
        /// </summary>
        public delegate void MouseEnd(Vector3 mousePosition);
        public static event MouseEnd OnMouseEndTriggered;

        /// <summary>
        /// While mouse button is down we keep track on mouseDown until its released
        /// </summary>
        public delegate void MouseDown(Vector3 mousePosition);
        public static event MouseDown OnMouseDownTriggered;
        #endregion

    }
}
