﻿using System.Collections;
using UnityEngine;

namespace CodeLord.CatVsDog
{
    //x : -21
    public class AICharacterController : MonoBehaviour
    {
        private Vector2 DirectHitVector = new Vector2(35f, 12f);

        private CharacterControllerBase myCharacter;
        private CharacterControllerBase oponentCharacter;
        private Vector3 oponentPosition;
        private SlingshotWeapon weaponController;
        private bool isMyTurn;

        private void Start()
        {
            RegisterToCharacterEvents();
            LocatePlayerOponent();
            FindWeaponAbility();
            CalculateHitVector();
        }
        
        private void FindWeaponAbility()
        {
            weaponController = this.GetComponentInChildren<SlingshotWeapon>();
        }

        private void RegisterToCharacterEvents()
        {
            if (!myCharacter)
                myCharacter = this.GetComponent<CharacterControllerBase>();

            myCharacter.OnTurnChanged += this.CharacterControllerBase_OnTurnChanged;
        }

        private void CharacterControllerBase_OnTurnChanged(bool myTurn)
        {
            MyTurn(myTurn);
        }

        private void CalculateHitVector()
        {
            Vector2 startPos = this.weaponController.slingshotView.transform.position;

            float time = Random.Range(1f, 2f);

            DirectHitVector = (Vector2)ProjectileHelper.ComputeVelocityToHitTargetAtTime(startPos, GamePlayManagerBase.Instance.player1.transform.position, -20f, time);

            GamePlayManagerBase.Instance.debugController.DebugCustom("Direct hit vector " + DirectHitVector, Utils.DebugController.Color.blue, this, true);
        }

        private void LocatePlayerOponent()
        {
            oponentCharacter = GamePlayManagerBase.Instance.player1;
            oponentPosition = oponentCharacter.transform.position;
        }

        public IEnumerator ThrowDelayed()
        {
            yield return new WaitForSeconds(2.5f);

            CalculateHitVector();

            myCharacter.OnDrag(DirectHitVector,true);
            weaponController.SetSlingshotColor(Color.red);
            weaponController.RotateToTarget((DirectHitVector),true);
            weaponController.FireWeapon(DirectHitVector, true);

            isMyTurn = false;

            yield return new WaitForSeconds(2f);

            weaponController.SetSlingshotVisibilityState(false);

            GamePlayManagerBase.Instance.SwitchTurn();
        }

        public void MyTurn(bool myTurn)
        {
            isMyTurn = myTurn;

            if(isMyTurn)
                StartCoroutine(ThrowDelayed());
        }
    }
}

