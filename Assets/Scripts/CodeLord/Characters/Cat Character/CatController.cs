﻿using UnityEngine;

namespace CodeLord.CatVsDog
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(KeyboardInput))]
    public class CatController : CharacterControllerBase
    {
        private void Reset()
        {
            SetColliderSettings();
        }
    }
}