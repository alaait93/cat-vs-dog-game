﻿using UnityEngine;

namespace CodeLord.CatVsDog
{
    public class CameraController : MonoBehaviour
    {
        // x : -5.7
        //ortho size : 10
        private Transform target;

        public Camera GetCamera()
        {
            return this.GetComponent<Camera>();
        }

        public void PlayIntroMotion()
        {
            target = GamePlayManagerBase.Instance.player1.transform;
        } 
    }
}