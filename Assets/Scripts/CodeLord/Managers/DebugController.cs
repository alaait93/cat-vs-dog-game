﻿using UnityEngine;

namespace CodeLord.Utils
{
    public class DebugController : MonoBehaviour
    {
        public bool enableDebugLog = true;
        public Debug debug = new Debug();
        public enum Color
        {
            red,green,blue,black,gray,yellow,cyan,magenta,white
        }
        public void DebugCustom(string message, Object context = null, bool richText = false)
        {

            if(enableDebugLog)
                debug.DebugCL(message, context, richText);
        }

        public void DebugCustom(string message, Color color, Object context = null, bool richText = false)
        {
            if (enableDebugLog)
                debug.DebugCL(message,color.ToString() ,context, richText);
        }

    }
}

