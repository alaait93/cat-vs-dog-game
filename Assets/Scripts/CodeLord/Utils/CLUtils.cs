﻿using UnityEngine;

namespace CodeLord.Utils
{
    public static class CLUtils
    {
        public static Vector3 ConvertWorldToScreenSpace(Vector3 worldPoint, Camera projectionCamera)
        {
            if(projectionCamera)
                 return projectionCamera.WorldToScreenPoint(worldPoint);

            return Vector3.zero;
        }

        public static Vector3 ConvertScreenToWorldPoint(Vector3 screenPoint, Camera projectionCamera)
        {
            if (projectionCamera)
                return projectionCamera.ScreenToWorldPoint(screenPoint);
            return Vector3.zero;
        }
         

        /// <summary>
        /// This is an extension for UnityEngine Debug.Log function by Code Lord
        /// </summary>
        /// <param name="debug"></param>
        public static void DebugCL(this UnityEngine.Debug debug, string message, Object context, bool bold = false)
        {
            if (message != null)
            {
                if (bold)
                    UnityEngine.Debug.Log("<b>" + message + "</b>", context);
                else
                    UnityEngine.Debug.Log("" + message + "", context);
            }

            else
                UnityEngine.Debug.Log(" Message cant be empty ", context);    
        }

        public static void DebugCL(this UnityEngine.Debug debug, string message, string color, Object context = null, bool bold = false)
        {
            if (message != null)
            {
                string colorCode = "";
                string boldEndTag = "</b>";
                string colorEndTag = "</color>";
                string boldFont = (bold ? "<b>" : "");


                colorCode = (color.ToString().Length > 0 ? ("<color=" + color.ToString() + ">") : "");

                if (colorCode.Length <= 0)
                    colorEndTag = "";

                if (boldFont.Length <= 0)
                    boldEndTag = "";


                UnityEngine.Debug.Log(boldFont + "" + colorCode + "" + message +""+ colorEndTag + "" + boldEndTag,context);
                
            }

            else
                UnityEngine.Debug.Log(" Message cant be empty ", context);
        }
    }
}
