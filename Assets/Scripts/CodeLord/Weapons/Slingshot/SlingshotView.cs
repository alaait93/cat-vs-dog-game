﻿using UnityEngine;
using DG.Tweening;

namespace CodeLord.CatVsDog
{
    public class SlingshotView : WeaponViewBase
    {
        public Transform weaponStandpoint;

        public void LookIntoTarget(Vector3 throwDirection, bool animate = false)
        {
            throwDirection.z = 0f;
            if (animate)
                this.transform.DORotate(throwDirection, 1f, RotateMode.FastBeyond360);
            //this.transform.right = Vector3.Lerp(this.transform.right, throwDirection,1f * Time.deltaTime);
            else
                this.transform.right = throwDirection;
        }

        public void SetTint(Color color)
        {
            spriteRenderer.color = color;
        }
    }
}