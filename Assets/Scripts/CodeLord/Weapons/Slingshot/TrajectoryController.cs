﻿using System.Collections.Generic;
using UnityEngine;

namespace CodeLord.CatVsDog
{
    public class TrajectoryController : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField]
        private GameObject dotPrefab = null;

        [SerializeField]
        [Range(20, 200)]
        private int numberOfDots = 20;

        [SerializeField]
        [Range(0.01f, 0.5f)]
        private float DOT_TIME_STEP = 0.01f;
        #endregion

        #region Private Fields
        private Vector2 LAUNCH_VELOCITY = new Vector2(20f, 80f);
        private Vector2 INITIAL_POSITION = Vector2.zero;
        private Vector2 GRAVITY = new Vector2(0f, -200f);

        private List<GameObject> trajectoryDots;
        private GameObject trajectoryPointsHolder;
        private bool dotColorDefined = false;
        #endregion

        public void CreateTrajectoryDotList()
        {
            trajectoryDots = new List<GameObject>();

            trajectoryPointsHolder = new GameObject("TrajectoryDots");
            trajectoryPointsHolder.transform.position = Vector3.zero;
            trajectoryPointsHolder.SetActive(false);

            for (int x = 0; x < numberOfDots; x++)
            {
                GameObject trajectoryDot = Instantiate(dotPrefab);

                trajectoryDot.transform.position = CalculatePosition(DOT_TIME_STEP * x);

                Vector3 newScale = new Vector3(trajectoryDot.transform.localScale.x - (DOT_TIME_STEP * x), trajectoryDot.transform.localScale.y - (DOT_TIME_STEP * x), trajectoryDot.transform.localScale.z - (DOT_TIME_STEP * x));

                Color alphaColor = Color.white;
                
                alphaColor = new Color(alphaColor.r, alphaColor.g, alphaColor.b, alphaColor.a - (DOT_TIME_STEP * x));

                trajectoryDot.transform.localScale = newScale;

                trajectoryDot.GetComponent<SpriteRenderer>().color = alphaColor;

                trajectoryDot.transform.parent = trajectoryPointsHolder.transform;

                trajectoryDots.Add(trajectoryDot);
            }
        }

        /// <summary>
        /// Show or hide trajectory lines
        /// </summary>
        /// <param name="visible"></param>
        public void SetTrajectoryDotListColorState(Color color)
        {
            if (!dotColorDefined)
            {
                dotColorDefined = true;
                for (int x = 0; x < trajectoryDots.Count; x++)
                {
                    float cachedDotAlphaColor = trajectoryDots[x].GetComponent<SpriteRenderer>().color.a;
                    trajectoryDots[x].GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, cachedDotAlphaColor);
                }
            }
        }

        public void SetTrajectoryDotListVisibilityState(bool visible = true)
        {
            trajectoryPointsHolder.SetActive(visible);
        }

        public void UpdateTrajectoryDotPosition()
        {
            for (int x = 0; x < trajectoryDots.Count; x++)
            {
                trajectoryDots[x].transform.position = CalculatePosition(DOT_TIME_STEP * x);
            }
        }

        public void UpdateTrajectoryInput(Vector2 initialVelocity, Vector2 initialPosition, Vector2 gravity)
        {
            LAUNCH_VELOCITY = initialVelocity;
            INITIAL_POSITION = initialPosition;
            GRAVITY = gravity;
        }

        private Vector2 CalculatePosition(float elapsedTime)
        {
            return GRAVITY * elapsedTime * elapsedTime * 0.5f + LAUNCH_VELOCITY * elapsedTime + INITIAL_POSITION;
        }

    }
}