﻿using UnityEngine;

namespace CodeLord.CatVsDog
{
    public class SlingshotWeapon : WeaponControllerBase
    {
        #region Inspector
        
        public GameObject weaponPrefab;
        public float Gravity = -20f;

        [HideInInspector]
        public TrajectoryController trajectoryController;
        #endregion
        public SlingshotView slingshotView;

        private float minimumSlingDragAmount = 0.5f;
        private Vector3 slingshotOriginPoint;
        private Vector3 throwDirection;
        private Vector3 mouseSensitivityOffset = new Vector3(-5f, -12f, 0f);
        private Vector2 slingshotBoundaries_X = new Vector2(1f,25f);
        private Vector2 slingshotBoundaries_Y = new Vector2(-20.5f, -4.2f);
        private bool trajectoryInited = false;
        #region Monobehaviour callbacks

        private void Awake()
        {
            slingshotView = this.GetComponentInChildren<SlingshotView>();
            trajectoryController = this.GetComponent<TrajectoryController>();
        }

        private void Start()
        {
            slingshotOriginPoint = slingshotView.transform.position;
            InitSlingshot();
        }
        #endregion

        public void InitSlingshot()
        {
            trajectoryController.UpdateTrajectoryInput(throwDirection, new Vector3(slingshotOriginPoint.x, slingshotOriginPoint.y), new Vector2(0f, Gravity));
            trajectoryController.CreateTrajectoryDotList();
        }
        /// <summary>
        /// Take the input direction and rotate slingshot towards that direction
        /// </summary>
        /// <param name="targetDirection"> The direction where the slingshot is going to rotate towards</param>
        /// <param name="reversedShot"> This is for AI or for none main player in other words the player standing on the left side who have a reversed direction</param>
        public override void RotateToTarget(Vector3 targetDirection, bool reversedShot = false)
        {
            // Check wether we reset drag to slingshot origin then hide it and prevent throw else keep moving the slingshot based on drag values
            if ((targetDirection.x - slingshotOriginPoint.x) >= minimumSlingDragAmount && !trajectoryInited)
            {
                trajectoryInited = true;
                SetSlingshotVisibilityState(true);
            }
            // Another check but on Y axis
            else if ((targetDirection.y - slingshotOriginPoint.y) >= minimumSlingDragAmount && (targetDirection.x - slingshotOriginPoint.x) >= 0 && !trajectoryInited)
            {
                trajectoryInited = true;

                SetSlingshotVisibilityState(true);
            }

            else
            {
                if (trajectoryInited)
                {
                    if ((targetDirection.x - slingshotOriginPoint.x) <= 0f)
                    {
                        trajectoryInited = false;
                        SetSlingshotVisibilityState(false);
                    }       
                }
            }

            // We added a sensitivity offset on each axis to make the slingshot directions more flexible since raw mouse position values will not allow us to drag everywhere inside the game field
            if (!reversedShot)
                targetDirection = targetDirection + mouseSensitivityOffset;
 
            throwDirection = targetDirection;

            // Prevent slingshot from moving in negative direction by limiting its X and Y values
            if (!reversedShot)
            {
                throwDirection.x = Mathf.Clamp(throwDirection.x, slingshotBoundaries_X.x, slingshotBoundaries_X.y);
                throwDirection.y = Mathf.Clamp(throwDirection.y, slingshotBoundaries_Y.x, slingshotBoundaries_Y.y);
            }


            // Update the Slingshot based on drag values
            if (!reversedShot)
            {
                // bind the view into target
                slingshotView.LookIntoTarget(throwDirection);
                UpdateSlingshot(throwDirection * -1f, slingshotOriginPoint, Gravity);
            }
            else
            {
                // Bind the view into target
                slingshotView.LookIntoTarget(throwDirection * -1f);
                UpdateSlingshot(throwDirection, slingshotOriginPoint, Gravity);
            }
        }

        public void UpdateSlingshot(Vector3 throwDirection, Vector2 slingshotOriginPoint, float gravity)
        {
            trajectoryController.UpdateTrajectoryInput(throwDirection, new Vector3(slingshotOriginPoint.x, slingshotOriginPoint.y), new Vector2(0f, gravity));
            trajectoryController.UpdateTrajectoryDotPosition();
        }
         
        public void SetSlingshotColor(Color trajectoryStartColor)
        {
            trajectoryController.SetTrajectoryDotListColorState(trajectoryStartColor);
        }

        public void SetSlingshotVisibilityState(bool visible = true)
        {
            trajectoryController.SetTrajectoryDotListVisibilityState(visible);
        }

        /// <summary>
        /// Instantiate and throw object/weapon towards throw direction
        /// </summary>
        /// <param name="throwDirection">The vector direction where we should throw our weapon</param>
        /// <param name="reveredShot">For AI or player standing on the left side so we opposite the X direction of the weapon </param>
        public override void FireWeapon(Vector3 throwDirection, bool reveredShot = false)
        {
            GameObject weapon = Instantiate(weaponPrefab);
            weapon.transform.position = slingshotOriginPoint;

            if (!reveredShot)
                throwDirection *= -1f; // because we need to throw object in the opposite direction of the slingshot

            weapon.GetComponent<Rigidbody2D>().velocity = (Vector2)throwDirection;
        }

        /// <summary>
        /// Keep track of throw inputs and throw object when condition is true, if its too close to slingshot origin then consider it as cancel and don't throw
        /// </summary>
        /// <param name="dragDirection"> direction to throw towards</param>
        /// <param name="fireTriggered">a delegate to keep track weither we are done or not to switch turns after throwing weapon </param>
        /// <param name="reversedShot"> For AI or player on the left so don't validate input values just throw it immediately </param>
        public override void CheckThrowAvailability(Vector3 dragDirection, CharacterControllerBase.FireTriggered fireTriggered, bool reversedShot = false)
        {
            if ((dragDirection.x - slingshotOriginPoint.x) >= minimumSlingDragAmount)
            {
                FireWeapon(throwDirection);
                fireTriggered?.Invoke(true);
            }

            else if ((dragDirection.y - slingshotOriginPoint.y) >= minimumSlingDragAmount && (dragDirection.x - slingshotOriginPoint.x) >= 0)
            {
                FireWeapon(throwDirection);
                fireTriggered?.Invoke(true);
            }
            else if (reversedShot)
            {
                FireWeapon(throwDirection);
                fireTriggered?.Invoke(true);
            }
            else
                fireTriggered?.Invoke(false);
        }
    }
}
