﻿using UnityEngine;

public class DestroyAfterAWhile : MonoBehaviour
{
    public float delay = 4f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyAfterDelay", delay);
    }

    private void DestroyAfterDelay()
    {
        Destroy(this.gameObject);
    }

}
